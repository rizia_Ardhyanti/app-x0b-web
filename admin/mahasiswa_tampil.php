<?php include 'header.php'; ?>
<?php
include 'koneksi.php';
$db = new database(); 
?>

<h3><span class="glyphicon glyphicon-user"></span>  Data Mahasiswa</h3>
<button style="margin-bottom:20px" data-toggle="modal" data-target="#myModal" class="btn btn-info col-md-2"><span class="glyphicon glyphicon-plus"></span>Tambah </button>
<br/>
<br/>
<div>
	<a style="margin-bottom:10px" href="" target="_blank" class="btn btn-default pull-right"><span class='glyphicon glyphicon-print'></span>  Cetak</a>
</div>
<form action="cari_act.php" method="get">
	<div class="input-group col-md-5 col-md-offset-7">
		<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search"></span></span>
		<input type="text" class="form-control" placeholder="Cari user di sini .." aria-describedby="basic-addon1" name="cari">	
	</div>
</form>
<br/>
<table class="table table-hover">
    <thead>
  <tr>
		<th>NO</th>
		<th>NIM</th>
		<th>Nama Mahasiswa</th>
		<th>Program Studi</th>
		<th>Photos</th>
		<th>Tanggal Lahir</th>
		<th>ALamat</th>
		<th>Jenis Kelamin</th>
		<th>Tindakan</th>
  </tr>
  </thead>
  <tbody>
  <?php
   $no = 1;
  foreach ($db->tampil_mahasiswa() as $data) { ?>
   <tr>
			<td><?php echo $no++; ?></td>
			<td><?php echo $data['nim']; ?></td>
			<td><?php echo $data['nama']; ?></td>
			<td><?php echo $data['nama_prodi']; ?></td>
			<td align="center"><?php echo "<img src='../images/$data[photos]' width='70' height='90' />";?></td>
			<td><?php echo $data['tangal_lahir']; ?></td>
			<td><?php echo $data['alamat']; ?></td>
			<td><?php echo $data['jenis_kelamin']; ?></td>
			<td>
				<a href="mahasiswa_det.php?nim=<?php echo $data['nim']; ?>" class="btn btn-info">Detail</a>
				<a href="mahasiswa_edit.php?nim=<?php echo $data['nim']; ?>" class="btn btn-warning">Edit</a>
				<a onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ??')){ location.href='proses.php?nim=<?php echo $data['nim']; ?>&aksi=hapus' }" class="btn btn-danger">Hapus</a>
			</td>
		</tr>
    </tbody>
    <?php
  }
  ?>
</table>
<!-- modal input -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Tambah Mahasiswa Baru</h4>
			</div>
			<div class="modal-body">
				<form action="mahasiswa_tambah.php" method="post">
					<div class="form-group">
						<label>NIM</label>
						<input name="nim" type="text" class="form-control" placeholder="NIM">
					</div>
					<div class="form-group">
						<label>Nama Mahasiswa</label>
						<input name="nama" type="text" class="form-control" placeholder="Nama Mahasiswa">
					</div>
					<div class="form-group">
						<label>Program Studi</label>
						<input name="prodi" type="text" class="form-control" placeholder="Program Studi">
					</div>
					<div class="form-group">
						<label>Foto</label>
						<input name="photos" type="text" class="form-control" placeholder="Upload foto">
					</div>
					<div class="form-group">
						<label>Alamat</label>
						<input name="alamat" type="text-area" class="form-control" placeholder="Alamat">
					</div>
					<div class="form-group">
						<label>Jenis Kelamin</label>
						<input name="jenis_kelamin" type="text" class="form-control" placeholder="Jenis Kelamin">
					</div>															

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<input type="submit" class="btn btn-primary" value="Simpan">
				</div>
			</form>
		</div>
	</div>
</div>
